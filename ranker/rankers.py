from sparql_manager.search_functions import get_label_query, get_labels_query
from utils.util_functions import edit_distance, remove_duplicates, \
    split_list, extract_result_values


class RankResult:

    def __init__(self, query, score=0):
        self.query = query
        self.score = score

    def __str__(self):
        return f"{self.query}\n\nScore: {self.score}"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.score == other.score

    def __gt__(self, other):
        return self.score > other.score


class SimpleRanker:

    def __init__(self, query_service):
        self.query_service = query_service
        # Find labels of all relevant iris
        self.label_cache = {}

    def form_label_cache(self, all_iris, num_labels_at_once=100):
        """ Forms label cache(key -> iri, value -> label)"""

        for iri_sub_list in split_list(all_iris, num_labels_at_once):
            query = get_labels_query(iri_sub_list)
            results = self.query_service.post(query)
            for row in results:
                iri, label = extract_result_values(row)
                self.label_cache[iri] = label

    def get_num_words_covered(self, query_iris, all_iris_dict):
        """
            Number of the words in the question which are
            covered by the query
        """
        covered_words = set()
        for iri in query_iris:
            for n_gram in all_iris_dict:
                if iri in all_iris_dict[n_gram]:
                    for word in n_gram:
                        covered_words.add(word)

        return len(covered_words)

    def _get_phrase(self, iri, all_iris_dict):
        """
            Gets the word or phrase the iri represents
            in the query
        """
        for n_gram in all_iris_dict:
            if iri in all_iris_dict[n_gram]:
                return ' '.join(n_gram)

    def get_resource_label(self, iri):
        if iri in self.label_cache:
            return self.label_cache[iri]

        # if for some reason the iri is not in the cache, try to find the label;
        # with additional request
        result = self.query_service.post_with_retry(get_label_query(iri))
        ret_val = [extract_result_values(row)[0] for row in result][0]
        # we add the label to cache
        self.label_cache[iri] = ret_val
        return ret_val

    def get_distance(self, query_iris, all_iris_dict):
        """
            The edit distance of the label of the resource and
            the word it is associated to
        """
        total_distance = 0
        for iri in query_iris:
            resource_label = \
                self.get_resource_label(iri)
            phrase = self._get_phrase(iri, all_iris_dict)
            total_distance += edit_distance(resource_label.lower(), phrase.lower())
        return total_distance

    def calculate_score(self, query, queries_dict, all_iris_dict):
        return 10 * self.get_num_words_covered(queries_dict[query], all_iris_dict) - \
               5 * self.get_distance(queries_dict[query], all_iris_dict)

    def rank_queries(self, queries_dict, all_iris_dict):
        """
            queries_dict: dict containing key: query, val: list of iris
            all_iris_dict: key: word n-gram, iri list
        """
        all_iris = remove_duplicates([item for sublist in all_iris_dict.values() for item in sublist])
        self.form_label_cache(all_iris)
        rank_results = []
        for query in queries_dict.keys():
            rank_results.append(RankResult(query=query,
                                           score=self.calculate_score
                                           (query, queries_dict, all_iris_dict)))

        rank_results.sort(reverse=True)
        return rank_results
