## Introduction
Project for the subject of Semantic Web, written in `Python`. It allows the user to query knowledge bases (DBPedia) using natural English language. Aside from the standard "wh" question semantic, the software also has support for interrogative (yes/no) questions. Currently the tool queries DBPedia and is limited to the English language, though support can be expanded to other knowledge bases and/or languages.
## Installation Instructions
Running the `install.bat` script should take care of all dependencies (assuming `pip` is properly configured and added to PATH). Alternatively it is also possible to run these commands:

`pip install nltk` <br/>
`pip install requests` <br/>
`pip install validators` <br/>
`pip install urlextract`<br/>
`pip install eventlet`<br/>

The `nltk` module requires further configuration. Being a natural language processing software it depends on certain text data files which have to be downloaded separately. The most efficient way to do this is to start a `python` interactive shell.
Afterwards, one should run these two lines:

`import nltk`<br/>
`ntlk.download()`<br/>

This will open a dialog where you can pick which data to download. It is recommended to pick the `select all` option. After the download finishes, the project should be ready for use.

## Usage Instructions
The simplest way to run the program is to run the following command:

`python main.py`

The program also supports optional arguments:
| Argument | Explanation | Default Value |
| ------ | ------ | ------ |
| --verbose | Whether to show debug/warning/info logs or not | False |
| --generator | Whether to use simple or complex generator (see Implementation overview for more details) | simple |

## Examples

The domain of the questions is not fixed, since the tool queries DBPedia. The user can ask traditional factoid questions:

![Example 1](images/example1.PNG) <br/>
_Example 1: Basic information about countries_

![Example 2](images/example2.PNG) <br/>
_Example 2: If there are multiple answers they will all be fetched_

It is also possible to phrase questions in a shortened way. The tool should be able to pick up the relevant entities nonetheless. Furthermore, the tool is not limited to entities (people, places, things, etc) as answers.

![Example 3](images/example3.PNG) <br/>
_Example 3: Non traditional question which has a literal as the answer_

Finally the tool is also able to detect interrogative forms using a combination of regexes (see _Implementation Overview_):

![Example 4](images/example4.PNG) <br/>
_Example 4: Interrogative form_

## Implementation Overview

![Concept](images/concept.png) <br/>
_Implementation concept_

The software is mostly organized as a pipeline, where each step takes the product of the previous step and transforms it accordingly. The input is the raw question string, while the output is the answer(s) in form of a string.

#### Question Classifier
_Question Classifier_ is a module which provides the method to classify the question as a binary (interogative yes/no) question, or regular question. The method returns a boolean which determines its type. It relies on regexes to so. The regexes check the question's beginning for prhrases like _Is_, _Are_,  _Isn't_,  _Does_, _Didn't_, etc. Furthermore they check for the _or_ in certain positions in the question, so the classifier doesn't  classify a comparative question as an interrogative.

Examples:

_Is Belgrade the capital of Serbia?_ - _True_ <br/>
_What is the capital of Serbia?_ - _False_ <br/>
_Is AMD or Intel better?_ - _False_ <br/>

#### Word Tokenizer
_Word Tokenizer_ relies on `nltk` to turn the raw question string into a series of tokens. It starts by converting the sentence to lowercase, and extracting words as tokens. It then applies the stop word and lemmatization filters, which remove stop words and replace the words with their base forms (according to a dictionary) respectively. Lemmatization was found to be favorable to word stemming.
The tokenizer also extracts all possible n-grams (where the n ranges from 1 to n).

Example:

Raw question: _What is the location of the Exit festival?_ <br/>
Filtered tokens: _location_, _exit_, _festival_ <br/>
N-grams: (_location_,), (_exit_,), (_festival_,), (_location_, _exit_), (_exit_, _festival_), (_location_, _exit_, _festival_)

These n-grams serve as the input for the next step.

#### SPARQL Service
_SPARQL service_ is a component which handles communication with DBPedia. It allows us to send SPARQL queries, and also parses outputs into objects which represent RDF resources and literals. However, its main purpose is to query all relevant International Resource Identifiers (IRIs) by using the list of n-grams as input. Each n-gram is put through a series of queries with equals and contains, with n-gram's contents being converted to label-like format. All query results are grouped by n-gram sequences. This means that the method returns a dictionary where an n-gram serves as a key, whereas the list of relevant IRIs relevant for that n-gram serve as value.

#### Query Generator
_Query Generator_ is a component which takes the information about the question type (whether it is a yes/no question) and the IRIs dictionary. It outputs SPARQL query candidates. If the question is a yes/no question, the software will generate ASK queries, otherwise SELECT queries will be generated. There are two types of generators:

_Simple Generator_ is a generator which can differentiate between properties and resources and can quickly generate all combinations, containing a single RDF triple pattern. The downside of this approach is that all combinations do not have to necessarily make sense. However, in that case, the tool fully relies on the ranker to sort the candidates out.

_Complex Generator_ relies on a smarter type of algorithm, which takes the distance between IRIs into account. The distance is defined as a number of steps from IRI to IRI. For example in this "graph" _r1_ _p1_ _r2_, the distance between _r1_ and _p1_ is 1, while the distance between _r1_ and _r2_ is 2. When going from the other side, the distance is considered to be negative. Since knowledge graphs tend to be directed, the path between _r1_ and _r2_ doesn't have to be same in both directions. Unfortunately, even with special optimized distance queries, getting the distance information from DBPedia has proved to be too slow to have any real-life application. An index structure would have to be formed on the computer doing the calculations.


#### Ranker

_Ranker_ takes generated queries and ranks them according to a score calculated from a linear combination of the following features:

- _Num words covered_: This feature determines how much of the tokenized question the query covers.
- _Levenshtein distance_: Calculates the sum of [Levenshtein](https://en.wikipedia.org/wiki/Levenshtein_distance) distances between the label of each IRI present in the query, and the n-gram the IRI corresponds to (extracted from the SPARQL service in a previous step).

#### Result Extractor

_Result Extractor_  is a component which goes through the ranked query candidates. It executes them, neatly extracts the result, and if it is non-empty, returns it as a label or a literal. 

## License
This project is available under the MIT license.
