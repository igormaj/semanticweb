import argparse


class Args:

    def __init__(self):
        self._parser = argparse.ArgumentParser()

        self._parser.add_argument('-vr', '--verbose',
                                  required=False,
                                  default=False,
                                  type=bool,
                                  help='Whether to show logs or not')

        self._parser.add_argument('-g', '--generator',
                                  required=False,
                                  default='simple',
                                  type=str,
                                  help='Whether to use simple or complex generator')

    def parse_args(self):
        return self._parser.parse_args()


arguments = Args().parse_args()
