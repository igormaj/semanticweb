import os
import pickle
from pathlib import Path

import nltk
import validators
from urlextract import URLExtract

extractor = URLExtract()
extractor.update()


def write_properties(rows, filename="../properties.txt"):
    file = open(filename, "w", encoding="utf-8")
    for row in rows:
        file.write(str(row) + "\n")
    file.close()


def extract_result_values(result):
    """
    :param result:sparql.RDFTerm subclass (one row returned by query)
    :return: values of the result, string(s), number(s) or other constant
    as tuple
    Result's len corresponds to the number of selected vars in select
    query
    """
    ret_val = []

    for i in range(len(result)):
        ret_val.append(result[i].value)

    return tuple(ret_val)


def remove_duplicates(a_list):
    return list(dict.fromkeys(a_list))


def is_url(text):
    return validators.url(text)


def is_property(text):
    return is_url(text) and ("/property/" in text or "/ontology/" in text)


def is_resource(text):
    return is_url(text) and "/resource/" in text


def is_variable(text):
    return text.isidentifier()


def is_ask_query(query):
    return query.strip().lower().startswith("ask")


def is_select_query(query):
    return query.strip().lower().startswith("select")


# https://en.wikipedia.org/wiki/Edit_distance
# https://en.wikipedia.org/wiki/Levenshtein_distance
def edit_distance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    return nltk.edit_distance(s1, s2)


# Split a python list into other “sublists” i.e smaller lists
def split_list(data, n):
    return [data[x:x + n] for x in range(0, len(data), n)]


def extract_urls(query_text):
    return extractor.find_urls(query_text)


def load_object(path, default=None):
    try:
        file = open(path,'rb')
        ret_val = pickle.load(file=file)
        file.close()
        return ret_val
    except IOError:
        return default


def write_object(obj, path):
    path_obj = Path(path)
    os.makedirs(path_obj.parent.absolute(), exist_ok=True)
    file = open(path, 'wb')
    pickle.dump(obj=obj, file=file)
    file.close()


if __name__ == '__main__':
    print(split_list("born year", "born"))
