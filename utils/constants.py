ALL_PROPERTIES_QUERY = """
    SELECT ?pred WHERE {
        ?pred a rdf:Property
    }
        ORDER BY ?pred
    """

COUNT_PROPERTIES_QUERY = """
select COUNT( ?property ) where {
 ?property a rdf:Property
}
"""