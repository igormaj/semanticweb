from utils.parse_args import arguments


def log(t, *messages):
    if arguments.verbose:
        print(t, ": ", *messages)


def debug_log(*messages):
    log('DEBUG', *messages)


def warning_log(*messages):
    log('WARNING', *messages)
