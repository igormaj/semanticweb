from nltk import ngrams
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import WordNetLemmatizer


class WordTokenizer:

    def __init__(self, language='english'):
        self.stop_words = set(stopwords.words(language))
        self.enlarge_stop_words()
        self.stemmer = SnowballStemmer(language)
        self.lemmatizer = WordNetLemmatizer()

    def enlarge_stop_words(self):
        """Common words in questions"""
        self.stop_words.add((
            'what',
            'give',
            'which',
            "is",
            "are"
        ))

    def tokenize(self, sentence):
        word_tokens = word_tokenize(sentence)
        filtered_sentence = [word.lower() for word in word_tokens if word.lower() not in self.stop_words and word.lower().isalnum()]
        return self.lemmatize(filtered_sentence)

    def _stem(self, token):
        return self.stemmer.stem(token)

    def stem(self, tokens):
        """ Normalizations, such as conversion to lower case or to base
            forms, such as “é,é,ê” to “e”, allow matching of slightly
            different forms
        """
        return [self._stem(token=token) for token in tokens]

    def _lem(self, token):
        return self.lemmatizer.lemmatize(token)

    def lemmatize(self, tokens):
        """ Normalizations, such as conversion to lower case or to base
            forms, such as “é,é,ê” to “e”, allow matching of slightly
            different forms
        """
        return [self._lem(token=token) for token in tokens]

    def get_ngrams(self, word_list, max_n=None):
        """This method gets 1...n grams. Default n is len(word_list)  """
        ret_val = []
        if max_n is None:
            max_n = len(word_list)

        for i in range(1, max_n+1):
            ret_val.extend(ngrams(word_list,i))

        return ret_val
