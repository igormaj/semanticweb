from sparql_manager.search_functions import get_label_query
from utils.log import debug_log, warning_log
from utils.util_functions import extract_result_values, is_resource, split_list, extract_urls


class BinaryResult:
    """
        Result for binary(yes/no) questions. if is_valid is false, it means
        that the question makes no sense i.e a resource doesn't contain the property
    """

    def __init__(self, is_valid, value):
        self.is_valid = is_valid
        self.value = value

    def __str__(self):
        if self.is_valid:
            return self.value
        return 'N/A'


class ResultExtractor:
    """Extracts answers to best ranked queries"""

    def __init__(self, query_service):
        self.query_service = query_service

    def extract_answer_text(self, result_value):
        """
            Extracts label if the answer is a resource, otherwise just
            returns it
        """
        if is_resource(result_value):
            label_query = get_label_query(result_value)
            results = self.query_service.post_with_retry(label_query)
            return [extract_result_values(result)[0] for result in results][0]

        return result_value

    def extract_first_anwer_with_results(self, rank_results, is_yes_no=False):
        for result in rank_results:
            answers = self.extract_answer(result, is_yes_no=is_yes_no)
            if answers:
                return answers

        return [f"No answer unfortunately...(filtered {len(rank_results)}) candidates"]

    def binary_query_valid(self, query):
        """
            An algorithm to find a way to determine whether
            the ASK query even makes sense
            (do the iris even have the properties we ask of them)
        """
        for triple in split_list(extract_urls(query), 3):
            if not self.query_service.has_property(triple[0], triple[1]):
                return False

        return True

    def extract_binary_question_answer(self, result):
        """Returns Yes/No/(N/A)"""
        if not self.binary_query_valid(result.query):
            warning_log("Binary query not applicable.")
            debug_log(result.query)
            return []

        results = self.query_service.post_with_retry(result.query)
        return [BinaryResult(True, "Yes." if results.hasresult() else "No.")]

    def extract_answer(self, result, is_yes_no=False):

        if is_yes_no:
            return self.extract_binary_question_answer(result)

        answers = []

        # Finding answers (IRIS or literals)
        query = result.query
        results = self.query_service.post_with_retry(query)

        for result in results:
            answers.append(self.extract_answer_text
                           (extract_result_values(result)[0]))

        if not answers:
            warning_log("No results for a ranked query.")
            debug_log(query)

        return answers
