from itertools import chain

from sparql_manager.patterns import pattern_1
from sparql_manager.search_functions import check_distance_query
from utils.log import debug_log
from utils.util_functions import remove_duplicates, is_property, is_resource, load_object, write_object, is_variable

DISTANCES_DATA_PATH = './data/distances.dat'


def get_pairs(element_list):
    for elem1 in element_list:
        for elem2 in element_list:
            if elem1 != elem2:
                yield elem1, elem2


def get_resources(iris):
    for iri in iris:
        if is_resource(iri):
            yield iri


def get_properties(iris):
    for iri in iris:
        if is_property(iri):
            yield iri


def get_l_k(patterns, k):
    """Yields triple pattern containing k triples only"""
    for pattern in patterns:
        if len(pattern) == k:
            yield pattern


class ComplexQueryGenerator:
    """
        R is a set of resources(IRIs)
        We consider the knowledge base (DBpedia, Wikidata, etc)
        to be a directed labeled graph G
        graph G = (V,E,f)
        V - non empty set (vertex set)
        E - Edge set which is a subset of (v,w)
        where v and w are part of vertex set V
        f - function which assigns a label p to each
        edge, labeled edge is e = (v,p,w)
    """

    def __init__(self, query_service):
        self.query_service = query_service
        # keeps calculated distances for further use
        # we load them if there is a file
        # so we can speed up distance calculation
        self.distances = load_object(DISTANCES_DATA_PATH, {})

        self.max_triple_patterns = 2

    def calculate_distance(self, iri, iri1):
        if (iri, iri1) not in self.distances:
            self.set_distance(iri, iri1, self.query_service
                              .calculate_distance(iri, iri1))
        if (iri1, iri) not in self.distances:
            self.set_distance(iri1, iri, - self.query_service
                              .calculate_distance(iri1, iri))

    def calculate_distances(self, all_iris):
        for iri in all_iris:
            for iri1 in all_iris:
                if iri != iri1:
                    self.calculate_distance(iri, iri1)

        # caching distances so that similar questions resolve faster
        # in the future
        write_object(self.distances, DISTANCES_DATA_PATH)

    def set_distance(self, elem1, elem2, distance):
        debug_log(elem1, elem2, distance)
        self.distances[(elem1, elem2)] = distance

    def get_distance(self, elem1, elem2):
        if (elem1, elem2) not in self.distances:
            return float('inf')

        return self.distances[(elem1, elem2)]

    def distance_is(self, elem1, elem2, d):

        if is_variable(elem1) or is_variable(elem2):
            debug_log(elem1, elem2, d)
            query = check_distance_query(elem1, elem2, d)
            return self.query_service.post_with_retry(query).hasresult()

        return self.get_distance(elem1, elem2) == d

    def generate(self, is_yes_no, all_iris_dict):
        all_iris = remove_duplicates([item for sublist in all_iris_dict.values() for item in sublist])
        self.calculate_distances(all_iris)
        triple_pattern_list = set()
        # variables in subject or object positions
        v_s_o = set()
        # variables in predicate positions
        v_p = set()
        self._generate(all_iris, triple_pattern_list, 0, v_s_o, v_p)
        debug_log(triple_pattern_list)
        return {}

    def _generate(self, iris, L, k, v_s_o, v_p):
        for s1 in chain(get_resources(iris), v_s_o, [f'x_{k}_1']):
            for s2 in chain(get_properties(iris), v_p, [f'x_{k}_2']):
                for s3 in chain(get_resources(iris), v_s_o, [f'x_{k}_3']):

                    if k == 0 and self.distance_is(s2, s3, -1) and \
                            self.distance_is(s1, s2, 1) and \
                            self.distance_is(s1, s3, 2):
                        L.add((s1, s2, s3))

                    for T in get_l_k(L, k):
                        b1, b2, b3, b4 = True, True, True, True
                        for triple in T:
                            t1, t2, t3 = triple
                            if not self.b1_condition(s1, s2, s3, t1, t2, t3):
                                b1 = False
                            if not self.b2_condition(s1, s2, s3, t1, t2, t3):
                                b2 = False
                            if not self.b3_condition(s1, s2, s3, t1, t2, t3):
                                b3 = False
                            if not self.b4_condition(s1, s2, s3, t1, t2, t3):
                                b4 = False

                        if b1 or b2 or b3 or b4:
                            temp_set = set()
                            temp_set.add(T)
                            temp_set.add((s1, s2, s3))
                            L.update(temp_set)

                            v_s_o.add((s1, s3))
                            v_p.add((s2,))

                        if k != self.max_triple_patterns:
                            self._generate(iris, L, k + 1, v_s_o, v_p)

    def b1_condition(self, s1, s2, s3, t1, t2, t3):
        """
            (s1 = t1 ∧ d(t1,s2) = 2 ∧ d(t1,s3) = 3 ∧ d(t2,s2) = −2
            ∧ d(t2,s3) = −3 ∧ d(t3,s2) = −3 ∧ d(t3,s3) = −4)
        """
        return s1 == t1 and self.distance_is(t1, s2, 2) and \
               self.distance_is(t1, s3, 3) and \
               self.distance_is(t2, s2, -2) and \
               self.distance_is(t2, s3, -3) and \
               self.distance_is(t3, s2, -3) and \
               self.distance_is(t3, s3, -4)

    def b2_condition(self, s1, s2, s3, t1, t2, t3):
        """
            (s1 = t3 ∧ d(t1,s2) = 3 ∧ d(t1,s3) = 4 ∧ d(t2,s2) = 2
            ∧ d(t2,s3) = 3 ∧ d(t3,s2) = 1 ∧ d(t3,s3) = 2)
        """
        return s1 == t3 and self.distance_is(t1, s2, 3) and \
               self.distance_is(t1, s3, 4) and \
               self.distance_is(t2, s2, 2) and \
               self.distance_is(t2, s3, 3) and \
               self.distance_is(t3, s2, 1) and \
               self.distance_is(t3, s3, 2)

    def b3_condition(self, s1, s2, s3, t1, t2, t3):
        """
             (s3 = t1 ∧ d(t1,s2) = −1 ∧ d(t1,s3) = −4 ∧ d(t2,s2) = −2
             ∧ d(t2,s3) = −3 ∧ d(t3,s2) = −1 ∧ d(t3,s3) = −2)
        """
        return s3 == t1 and self.distance_is(t1, s2, -1) and \
               self.distance_is(t1, s3, -4) and \
               self.distance_is(t2, s2, -2) and \
               self.distance_is(t2, s3, -3) and \
               self.distance_is(t3, s2, -1) and \
               self.distance_is(t3, s3, -2)

    def b4_condition(self, s1, s2, s3, t1, t2, t3):
        """
             (s3 = t3 ∧ d(t1,s2) = −3 ∧ d(t1,s3) = 2 ∧ d(t2,s2) = −2
             ∧ d(t2,s3) = −1 ∧ d(t3,s2) = −1)
        """
        return s3 == t3 and self.distance_is(t1, s2, -3) and \
               self.distance_is(t1, s3, 2) and \
               self.distance_is(t2, s2, -2) and \
               self.distance_is(t2, s3, -1) and \
               self.distance_is(t3, s2, -1)


class SimpleQueryGenerator:

    def __init__(self):
        pass

    def add_yes_no_pattern_1(self, queries_dict_ret_val, pair, property):
        queries_dict_ret_val[pattern_1(True, None, pair[0], property, pair[1])] = [
            pair[0],
            property,
            pair[1]
        ]

        queries_dict_ret_val[pattern_1(True, None, pair[1], property, pair[0])] = [
            pair[1],
            property,
            pair[0]
        ]

    def add_pattern_1(self, queries_dict_ret_val, resource, property):
        queries_dict_ret_val[pattern_1(False, 'x', resource, property, 'x')] = [
            resource,
            property,
        ]

        queries_dict_ret_val[pattern_1(False, 'x', 'x', property, resource)] = [
            property,
            resource,
        ]

    def generate(self, is_yes_no, all_iris_dict):
        """ Generates a dict, where key is the query itself, and value is a
            list of IRIs used in the query
        """
        all_iris = remove_duplicates([item for sublist in all_iris_dict.values() for item in sublist])
        all_resources = []
        all_properties = []
        for iri in all_iris:
            if is_property(iri):
                all_properties.append(iri)
            if is_resource(iri):
                all_resources.append(iri)

        queries_dict_ret_val = {}

        for prop in all_properties:
            if is_yes_no:
                for pair in get_pairs(all_resources):
                    self.add_yes_no_pattern_1(queries_dict_ret_val, pair, prop)
            else:
                for resource in all_resources:
                    self.add_pattern_1(queries_dict_ret_val, resource, prop)

        return queries_dict_ret_val
