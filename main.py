from classifiers.question_classifier import BasicQuestionClassifier
from generators.query_generator import SimpleQueryGenerator, ComplexQueryGenerator
from lexer.tokenizer import WordTokenizer
from ranker.rankers import SimpleRanker
from result_extractor.result_extractor import ResultExtractor
from sparql_manager.sparql_query_service import SPARQLService
from utils.log import debug_log
from utils.parse_args import arguments


if __name__ == '__main__':
    word_tokenizer = WordTokenizer()
    query_service = SPARQLService('http://dbpedia.org/sparql')
    question_classifier = BasicQuestionClassifier()

    query_generator = ComplexQueryGenerator(query_service) if \
        arguments.generator == 'complex' \
        else SimpleQueryGenerator()

    ranker = SimpleRanker(query_service)
    result_extractor = ResultExtractor(query_service)
    while True:

        question = input("Enter a question: ")
        is_yes_no = question_classifier.is_yes_no(question)
        n_gram_list = word_tokenizer.get_ngrams(word_tokenizer.tokenize(question))
        debug_log(n_gram_list)
        all_iris_dict = query_service.search_all_iris(n_gram_list)

        queries_dict = query_generator.generate(is_yes_no, all_iris_dict)
        ranked_results = ranker.rank_queries(queries_dict, all_iris_dict)

        for ans_label in result_extractor.extract_first_anwer_with_results(ranked_results,
                                                                           is_yes_no=is_yes_no):
            print(ans_label)



