import re

verbs = ["have",
         "haven't"
         "has",
         "hasn't"
         "did",
         "didn't"
         "had",
         "hadn't"
         "do",
         "doesn't"
         "am",
         "am not",
         "is",
         "isn't",
         "aren't",
         "are",
         "can",
         "can't"
         "could",
         "couldn't"
         "will",
         "won't"
         "would",
         "wouldn't"
         "does",
         "doesn't"
         "was",
         "wasn't"
         "were",
         "weren't"
         "may",
         "may not"
         "might",
         "might not"
         "should",
         "shouldn't"
         "ought",
         "oughtn't"
         "must",
         "mustn't"
         "shall",
         "shan't"
         ]

# By sorting the longer verbs (such as negations have priority)
verbs.sort()


class BasicQuestionClassifier:
    """
       Basic question classifier which uses a regex to classify
       whether the questions is yes/no
    """

    def __init__(self):
        # \b is word boundary, basically match whole word only
        self.regex = r'(' + '|'.join([r'(\b' + verb + r'\b)' for verb in verbs]) + ')'
        # to filter out questions like:  Is entity a or entity b better?
        self.or_regex = r'\bor\b'

    def is_yes_no(self, question):
        question = question.lower().strip()
        return re.match(self.regex, question) is not None \
               and re.search(self.or_regex, question) is None


if __name__ == '__main__':
    classifier = BasicQuestionClassifier()
    while True:
        print(classifier.is_yes_no(input("Enter a question: ")))
