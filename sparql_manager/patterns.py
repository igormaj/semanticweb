from utils.util_functions import is_url, is_variable


def element_pattern(element):
    if is_url(element):
        return f"<{element}>"
    if is_variable(element):
        return f"?{element}"
    return element


def select_or_ask_pattern(yes_no, var):
    if yes_no:
        return "ASK"
    return f"SELECT ?{var}"


def pattern_1(yes_no, var, s1, s2, s3):
    return f"""
    {select_or_ask_pattern(yes_no, var)}
    WHERE {{ {element_pattern(s1)} {element_pattern(s2)} {element_pattern(s3)} . }}
    """


def pattern_2(yes_no, var, s1, s2, s3, s4, s5, s6):
    return f"""
    {select_or_ask_pattern(yes_no, var)}
    WHERE {{ 
        {element_pattern(s1)} {element_pattern(s2)} {element_pattern(s3)} .
        {element_pattern(s4)} {element_pattern(s5)} {element_pattern(s6)} . 
    }}
    """


def pattern_3(var, iri):
    return f"""
    SELECT ?{var}
    WHERE {{ VALUES ?{var} {element_pattern(iri)} . }}
    """


def pattern_4(var, iri, iri1):
    return f"""
    SELECT ?{var}
    WHERE {{ VALUES ?{var} {element_pattern(iri)} .
    {element_pattern(iri)} ?p {element_pattern(iri1)} . }}
    """


if __name__ == '__main__':
    # Is Podgorica capital of Montenegro?
    print(pattern_1(True, None, 'http://dbpedia.org/resource/Montenegro', 'http://dbpedia.org/property/capital',
                    'http://dbpedia.org/resource/Podgorica'))

    # What is the capital of Montenegro?
    print(pattern_1(False, 'capital', 'http://dbpedia.org/resource/Montenegro', 'http://dbpedia.org/property/capital',
                    'capital'))
