import requests

from sparql_manager import sparql
from sparql_manager.search_functions import all_search_query_exact, all_search_query_contains, db_pedia_lexicalizations, \
    all_iri_properties, property_search_query, edge_exists_query,\
    distance_query
from utils.util_functions import remove_duplicates, extract_result_values, is_resource
import time


class SPARQLService:

    def __init__(self, endpoint):
        self.endpoint = endpoint

    def post(self, query, endpoint=None):
        """For SPARQL endpoints requiring POST"""
        if not endpoint:
            endpoint = self.endpoint
        return sparql.query(endpoint, query)

    def post_with_retry(self, query, endpoint=None):
        """For SPARQL endpoints requiring POST"""
        if not endpoint:
            endpoint = self.endpoint
        while True:
            try:
                return sparql.query(endpoint, query)
            except:
                time.sleep(1)
                continue

    def get(self, query_param_name, query, endpoint=None):
        """For SPARQL endpoints requiring GET"""
        if not endpoint:
            endpoint = self.endpoint
        r = requests.get(endpoint, params={'format': 'json', query_param_name: query})
        if r.status_code >= 400:
            raise Exception('Error code: ' + str(r.status_code) + ', ' + str(r.reason) + '\n' +
                            str(r.text))

        data = r.json()
        return data

    def get_rows(self, result):
        """For POST methods"""
        return [extract_result_values(row)[0] for row in result]

    def get_connected_resources(self, results):
        """Packs connected resources neatly into a list"""
        resources = []
        for result in results:
            try:
                potential_resource = extract_result_values(result)[1]
                if is_resource(potential_resource):
                    resources.append(potential_resource)
            except:
                pass
        return resources

    def is_correct(self, result):
        """For ASK queries"""
        return result.hasresult()

    def get_lexicalizations(self, results):
        lexicalizations = []
        for iri in results:
            lexicalizations.extend(self.get_rows(self.post(db_pedia_lexicalizations(iri))))

        return lexicalizations

    def has_property(self, iri, prop):
        """
        :param iri: resource to determine which has the property
        :param prop: property id
        :return: bool
        """
        results = self.post_with_retry(all_iri_properties(iri))
        all_resource_properties = [extract_result_values(result)[0] for result in results]
        return prop in all_resource_properties

    def edge_exists(self, iri, prop, iri1):
        """
        Tells us if there exists an edge e =(iri,prop,iri1)
        :param iri: resource to determine has property
        :param prop: property
        :param iri1: value of property
        :return: bool whether the edge exists
        """
        result = self.post_with_retry(edge_exists_query(iri,prop,iri1))
        return result.hasresult()

    def calculate_distance(self, resource1, resource2):
        results = self.post_with_retry(distance_query(resource1, resource2))
        # we have to iterate like this, but due to the nature
        # of the query(counts) there is only one result
        for result in results:
            result = extract_result_values(result)
            for i in range(len(result)):
                if result[i] != '0':
                    return i+1

        return float('inf')

    def search_all_iris(self, n_gram_list):
        """
            Searches all possible IRIs according to the n gram list
            returns one result row dict (n_gram_key, value list of IRIs)
        """
        ret_val = {}
        for n_gram in n_gram_list:
            search_term = ' '.join(n_gram)
            results = self.get_rows(self.post(all_search_query_exact(search_term)))
            results.extend(self.get_rows(self.post(all_search_query_contains(search_term))))
            results.extend(self.get_rows(self.post(property_search_query(search_term))))
            #results.extend(self.get_lexicalizations(results))
            ret_val[n_gram] = remove_duplicates(results)

        return ret_val


if __name__ == '__main__':
    service = SPARQLService('http://dbpedia.org/sparql')
    now = time.time()
    print(service.calculate_distance("http://dbpedia.org/resource/Montenegro", "http://dbpedia.org/resource/Croatia"))
    print(time.time() - now)
