from sparql_manager.patterns import element_pattern


def property_search_query(property_name):
    """
        Case insensitive search
        the union part is because a simple
        rdf:property doesn't yield properties which form
        non empty result queries
    """
    return f"""
    SELECT ?pred WHERE {{
        {{ 
            {{ ?pred a owl:ObjectProperty }} UNION
            {{ ?pred a owl:FunctionalProperty }}
        }} .
        ?pred rdfs:label ?label

        FILTER contains(lcase(?label),lcase("{property_name}"))
        FILTER langMatches(lang(?label),'en')
    }}
        ORDER BY ?pred
    """


def resourcefy_name(name):
    """Converts name to be more resource like:
       replaces spaces with _, every word is capitalized"""
    return "_".join([elem[0].upper() + elem[1:].lower() for elem in name.strip().split(" ")])


def labelize_name(name):
    """Converts name to be more resource like:
       replaces spaces with _, every word is capitalized"""
    return " ".join([elem[0].upper() + elem[1:].lower() for elem in name.strip().split(" ")])


def all_search_query_exact(name, limit=10):
    """
        Concept entities are disregarded (IRI's containing Category:SomeName)
        because they often do not bring anything useful
    """
    name = labelize_name(name)
    return f"""
        SELECT ?iri WHERE {{
            ?iri rdfs:label "{name}"@en .
            MINUS {{ ?iri rdf:type skos:Concept . }}
        }}
        ORDER BY DESC(str(?iri))
        LIMIT {limit}
        """


def all_search_query_contains(name, limit=10):
    """ Case insensitive search of hopefully all IRIs
        Bif is DBPEDIA specific.
        Concept entities are disregarded (IRI's containing Category:SomeName)
        because they often do not bring anything useful
    """
    name = resourcefy_name(name)
    return f"""
    PREFIX bif:<bif:>

    SELECT ?iri WHERE {{
        ?iri rdfs:label ?label .
        ?label bif:contains "{name}"@en .
        MINUS {{ ?iri rdf:type skos:Concept . }}
    }}
    ORDER BY DESC(str(?iri))
    LIMIT {limit}
    """


def db_pedia_lexicalizations(iri, limit=10):
    return f"""

        SELECT distinct ?lex WHERE {{
            {{
                ?lex  dbo:wikiPageRedirects <{iri}>
            }} 
            UNION
            {{
                ?lex  dbo:demonyn <{iri}>
            }}
            UNION
            {{
                <{iri}>  dbo:demonyn ?lex
            }}
             UNION
            {{
                <{iri}> dbo:wikiPageRedirects  ?lex
            }}
        }}
        LIMIT {limit}
        """


def all_iri_properties(iri):
    """
        Gets all properties of an IRI
        If a property has multiple values

    """
    return f"""
            select distinct ?property ?value {{
            <{iri}> ?property ?value
        }}
    """


def edge_exists_query(r, p, r1):
    """
        Checks if the edge exists
    """
    return f"""
            ASK {{
            <{r}> <{p}> <{r1}>
        }}
    """


# allows us to determine distances between (1-4)
def distance_query(elem1, elem2):
    return f"""
             SELECT count(?one_r1), count(?two_p1), count(?three_r2), count(?four_p2) WHERE{{
                
                {{   <{elem1}> ?four_p1 ?four_r1 .
                     ?four_r1 ?four_p2 <{elem2}>
                }}
                
                UNION
                {{  <{elem1}> ?three_p1 ?three_r1 .
                    ?three_r1 <{elem2}> ?three_r2 
                }}
                
                UNION
                {{  <{elem1}> ?two_p1 <{elem2}> }}
                
                UNION 
                {{ <{elem1}> <{elem2}> ?one_r1 }}
            }}
            """


def distance_1_query(elem1, elem2):
    return f"""
                ASK {{
                {element_pattern(elem1)} {element_pattern(elem2)} ?r1
            }}
        """


def distance_2_query(elem1, elem2):
    return f"""
                ASK {{
                {element_pattern(elem1)} ?p1 {element_pattern(elem2)}
            }}
        """


def distance_3_query(elem1, elem2):
    return f"""
                ASK {{
                {element_pattern(elem1)} ?p1 ?r1 .
                ?r1 {element_pattern(elem2)} ?r2
            }}
        """


def distance_4_query(elem1, elem2):
    return f"""
                ASK {{
                {element_pattern(elem1)} ?p1 ?r1 .
                ?r1 ?p2 {element_pattern(elem2)}
            }}
        """


check_distance_dict = {
    1: distance_1_query,
    2: distance_2_query,
    3: distance_3_query,
    4: distance_4_query
}


def check_distance_query(elem1, elem2, d):
    """
        Help method which just returns a query
        which can be used to check if the distance
        between elements is indeed d
    """
    if d < 0:
        return check_distance_dict[abs(d)](elem2, elem1)
    return check_distance_dict[d](elem1, elem2)


# https://stackoverflow.com/questions/38349975/sparql-how-to-obtain-label-in-available-languages-if-first-option-is-not-availa
def get_label_query(iri):
    """
        This query allows us to get the entity label
        in English if available, otherwise in first available language
    """
    return f"""SELECT DISTINCT str(?label) {{
        optional {{
            <{iri}> rdfs:label ?label .
            filter langMatches( lang(?label), "EN" )
        }}
        optional {{
            <{iri}> rdfs:label ?label .
        }}
    }}
    LIMIT 1
    """


def get_labels_query(iris):
    """
        This query allows us to get the entities label(s)
        Unlike other similar queries we need duplicates here,
        because some iris may have the same labels. Also, order
        is not kept, so we should return iri as well
    """
    return f"""SELECT ?iri, str(?label) {{
        {' UNION '.join([f'{{ VALUES ?iri {{ <{iri}> }} . ?iri rdfs:label ?label }}' for iri in iris])} .
        filter langMatches( lang(?label), "EN" )
    }}
    """


class IRINode:

    def __init__(self, iri, parent=None):
        self.iri = iri
        self.parent = parent

    def __eq__(self, other):
        return self.iri == other.iri

    def __str__(self):
        return str(self.iri)


if __name__ == '__main__':
    # print(extract_urls(db_pedia_lexicalizations('http://dbpedia.org/resource/Stalin')))
    print(get_labels_query(['http://dbpedia.org/resource/Josip_Broz_Tito',
                            'http://dbpedia.org/resource/Podgorica',
                            'http://dbpedia.org/resource/London',
                            'http://dbpedia.org/resource/Yugoslavia']))
